# Comix Crusher - Crusher Class and sundries
# does all the hard work
import gc
import os
import sys
import threading
import tempfile
import re

import gobject
import gtk
import pango

import gui
import archive
import zipfile

from const import *

class Crusher():
	"""
	The Comic book crusher
	Create one, set width() and widewidth(), then call:
		- crush_file() to crush one file
		- crush_files() to crush a bunch of files
		- chrush_in_thread to crush a bunch of files in a thread
		
	It's efficient - cleans up after itself and tries to only extract one file at a time
		(doesn't work like that at the moment with rars)
		
	It won't store a page in the destination file which is larger than the original.
		(TODO: optionally: revert to original or scale back jpg quality)
		
	"""
	
	IDLE,BUSY,ERROR,OK = range(4)
	@staticmethod
	def state_str(state):
		if state == Crusher.IDLE:
			return "Idle"
		elif state == Crusher.BUSY:
			return "Busy"
		elif state == Crusher.ERROR:
			return "Error"
		elif state == Crusher.OK:
			return "OK"
		return "You're not supposed to be here!"

	INFO,WARNING,ERROR=range(3)
	@staticmethod
	def level_str(level):
		if level == Crusher.INFO:
			return "Info"
		elif level == Crusher.WARNING:
			return "Warning"
		elif level == Crusher.ERROR:
			return "Error"
		return "WTF"

	def __init__(self):
		self._status = Crusher.BUSY
		self._statustext="Init"
		#we'll use the comix ectractor but not the filehandler
		# we will not let the extractor use threads - we'll
		# manage our own thread
		self.extractor = archive.Extractor()
		#our crusher thread:
		self.thread=None
		#a lock for thread-safe property access:
		self.lock = threading.RLock()

		self._pages=[]
		self._pages_processed=0
		self._total_pages_processed=0
		self._pages_processed_this_file=0
		self._pages_in_file = 0
		self._current_file = ""
		self._files_processed = 0
		self._files_to_process = 0
		self._current_filename = ""
		self._crushed=0
		self._skipped=0
		self._discarded=0
		self._src_size=0
		self._dest_size=0
		self._total_in = 0
		self._total_out = 0
		self._pixbuf = None

		self._width=820
		self._jpg_quality=75
		self._keep_nonimages=False
		self._cancelled=False
		self._widewidth=1600

		self._log = []
		#log messages since state() was last called:
		self._statelog = []

		#init finished:
		self.status(Crusher.IDLE)
		self.statustext("Idle")

	def log(self,msg,level=None):
		with self.lock:
			if level == None:
				level = Crusher.INFO
			print "%s: %s" % (Crusher.level_str(level),msg)
			itm={
				"level": level,
				"message": msg
			}
			self._log.append(itm)
			self._statelog.append(itm)

	def clear_pixbuf(self):
		with self.lock:
			self._pixbuf = None

	def width(self,val=None):
		with self.lock:
			if val != None:
				self._width=int(val)
			return self._width

	def widewidth(self,val=None):
		with self.lock:
			if val != None:
				self._widewidth=float(val)
			return self._widewidth

	def cancelled(self,val=None):
		with self.lock:
			if val != None:
				self._cancelled=int(val)
			return self._cancelled

	def jpg_quality(self,val=None):
		with self.lock:
			if val != None:
				self._jpg_quality=int(val)
			return self._jpg_quality

	def keep_nonimages(self,val=None):
		if val != None:
			self._keep_nonimages=bool(val)
		return self._keep_nonimages

	def statustext(self,val=None):
		with self.lock:
			if val != None:
				self._statustext=val
				if DEBUG:
					print "  %s" % val
			return self._statustext

	def status(self,val=None):
		with self.lock:
			if val != None:
				self._status=val
				if DEBUG:
					print "Status: %s" % (Crusher.state_str(val))
			return self._status

	def state(self):
		"""
		Returns a struct containing the current state:
		status: ready/busy/error/etc
		we get and release a single lock to populate the
			struct for efficiency
		"""
		ret={}
		with self.lock:
			ret={
				"status": self._status,
				"statustext": self._statustext,
				"pages_processed": self._pages_processed,
				"pages_in_file": self._pages_in_file,
				"current_file": self._current_file,
				"crushed": self._crushed,
				"skipped": self._skipped,
				"discarded": self._discarded,
				"src_size": self._src_size,
				"dest_size": self._dest_size,
				"pixbuf":self._pixbuf,
				"files_processed":self._files_processed,
				"files_to_process":self._files_to_process,
				"current_filename":self._current_filename,
				"total_in":self._total_in,
				"total_out":self._total_out,
				"logs": self._statelog,
			}
			self._statelog=[]
		return ret

	def crush_in_thread(self,files):
		#start a thread to process a bunch of files
		# files is a bunch of source -> dest pairs
		# if DISABLE_THREADING:
		#	self.crush_files(files)
		#	return False
		self.thread = threading.Thread(target=self.crush_files,args=([files]))
		self.thread.setDaemon(False)
		self.thread.start()

	def wait(self):
		self.thread.join()

	def crush_files(self,files):
		#crush a bunch of files.
		# files should be a list of tuples: [(src.dest).(src,dest)]
		with self.lock:
			self._files_processed = 0
			self._files_to_process = len(files)
			self._total_in = 0
			self._total_out = 0

		for item in files:
			src=item['src']
			dest=item['dst']

			if self.cancelled():
				return False

			self.crush_file(src,dest,False) #don't set OK state
			with self.lock:
				self._files_processed = self._files_processed + 1
				self._total_in = self._total_in + self._src_size
				self._total_out = self._total_out + self._dest_size
				self._pages_processed=0 #to prevent a jumpy 'overall progress' bar

		self.set_ok()

	def crush_image(self,filename):
		"""
		Crush an image file intelligently and save a copy, returning the
		 filename. In some cases, may return the original file unmolested
			- Normal comic pages will be scaled to self.width(), keeping
				aspect ratio
			- Wide images will be scaled to no wider than self.width() *
				self.wide_image_scale
			- images will be saved as a jpeg using self.jpg_quality()
				as a starting point: quality will reduce incrementally
				if output is larger than input
		"""

		return filename

	def crush_file(self,filename,dest_file=None,set_ok = True):
		"""
		Outline:
		1. Open file, get filenames
		2. open dest zipfile
		3. for each file:
			a. extract to tempfile, load image
			b. crush, add to dest
			c. remove tempfile
			d. gc.collect()
		"""
		self.log( "Crushing '%s'..." % filename)

		if dest_file is None:
			(root,ext) = os.path.splitext(filename)
			dest_file="%s-crushed.cbz" % root

		self.log( "Destination: '%s'" % dest_file)

		if not os.path.isfile(filename):
			gui.GtkOKDialog.show(None,"ERROR: '%s' does not exist!" % filename)
			return False

		if not os.access(filename, os.R_OK):
			gui.GtkOKDialog.show(None,"ERROR: no permission to access '%s'!" % filename)
			return False

		self.status(Crusher.BUSY)

		tmpdir = tempfile.gettempdir()
		#set up extractor
		self.extractor.setup(filename,tmpdir)
		#get file listing
		pages = self.extractor.get_files()

		#set up destination archive:
		try:
			#create parent dir if necessary
			archive.backtick("mkdir -p \"%s\"" % os.path.dirname(dest_file))
			zfile = zipfile.ZipFile(dest_file, 'w')
		except Exception:
			print '! Could not create archive', dest_file
			return False


		alphanumeric_sort(pages)
		cp=0
		#new width:
		nw=self.width()
		tw = int(self.widewidth())

		ok=True

		jpg_quality = "%1d" % self.jpg_quality()
		self.log("JPEG Quality: %s" % jpg_quality)
		
		with self.lock:
			self._src_size=0
			self._dest_size=0
			self._pages=pages
			self._pages_in_file = len(pages)
			self._pages_processed = 0
			self._current_filename = filename

		for page in pages:
			if self.cancelled():
				self.statustext("Cancelled!")
				self.status(Crusher.ERROR)
				ok = False
				break

			add=True
			cp=cp+1
			bn=os.path.basename(page)
			dst=False
			if bn:
				self.statustext("Extracting")

			if bn == "":
				with self.lock:
					self._pages_processed = self._pages_processed + 1
					self._total_pages_processed = self._total_pages_processed + 1
				continue #no file. note that we stop after
				#	extract so the dir structure in archive is created

			self.extractor._extract_file(page)

			tmp=os.path.join(tmpdir,page)
			ofs=os.path.getsize(tmp)
			ds=ofs
			with self.lock:
				self._current_file = bn

			if is_image_file(tmp):
				#load temp file:
				self.statustext("Loading")
				try:
					pb = gtk.gdk.pixbuf_new_from_file(tmp)
					ow=pb.get_width()
					oh=pb.get_height()
					got_image = True
				except:
					self.log("Error loading '%s', adding as-is" % tmp,Crusher.WARNING)
					got_image = False

				if got_image:
					if oh > ow: #tall image
						if ow > nw:
							#scale
							nh=int((oh/float(ow))*nw)
							self.statustext("Scaling %1dx%1d -> %1dx%1d" % (ow,oh,nw,nh))
							#we use the best interpolation to make the image as clear as possible
							pb = pb.scale_simple(nw,nh,gtk.gdk.INTERP_HYPER)
							#self.statustext("Saving")
							d=os.path.dirname(tmp)
							base,ext=os.path.splitext(tmp)
							dst=os.path.join(d,base+"-crushed.jpeg")
							pb.save(dst, "jpeg", {"quality":jpg_quality})
							fs=os.path.getsize(dst)
							if fs > ofs:
								#output larger than input!
								self.log("Output larger than input, reverting!")
								with self.lock:
									#self._dest_size=self._dest_size+ofs
									self._skipped=self._skipped+1
							else:
								#print "      result is %1.2fMB" % (float(fs)/1024/1024)
								with self.lock:
									ds=fs
									#self._dest_size=self._dest_size+fs
									self._crushed=self._crushed+1
								#cleanup tempfile now
								os.remove(tmp)
								tmp=dst

						else:
							self.statustext("(too small)")
							with self.lock:
								self._skipped=self._skipped+1
								#self._dest_size=self._dest_size+ofs
					else:	#wide image
						#self.statustext("Wide Image")
						if ow > tw:
							nh=int((oh/float(ow))*tw)
							self.statustext("Scaling %1dx%1d -> %1dx%1d" % (ow,oh,tw,nh))
							#we use the best interpolation to make the image as clear as possible
							pb = pb.scale_simple(tw,nh,gtk.gdk.INTERP_HYPER)
							#self.statustext("Saving")
							d=os.path.dirname(tmp)
							base,ext=os.path.splitext(tmp)
							dst=os.path.join(d,base+"-crushed.jpeg")
							pb.save(dst, "jpeg", {"quality":jpg_quality})
							fs=os.path.getsize(dst)
							#print "      result is %1.2fMB" % (float(fs)/1024/1024)
							with self.lock:
								ds=fs
								#self._dest_size=self._dest_size+fs
								self._crushed=self._crushed+1
							#cleanup tempfile now
							os.remove(tmp)
							tmp=dst

					with self.lock:
						self._pixbuf = pb

			else: #not an image
				#if keep mode, keep
				if self.keep_nonimages():
					with self.lock:
						ds=ofs
						#self._dest_size=self._dest_size+ofs
				else:
					self.statustext("Skipping non-image file")
					add=False

			if add: #set add=False to not add file to archive
				try:
					self.statustext("Archiving")
					zfile.write(tmp, page, zipfile.ZIP_DEFLATED)
				except Exception:
					print '! Could not add file %s to add to %s, aborting...' % (
						path, self._archive_path)
					zfile.close()
					try:
						os.remove(self._archive_path)
					except:
						pass
					return False

			with self.lock:
				self._src_size=self._src_size+ofs
				self._dest_size=self._dest_size+ds
				"""
				self.log("%1d pages processed: %s -> %s (%1.2f%%)" % (
					cp,
					size_str(self._src_size),
					size_str(self._dest_size),
					self._dest_size / float(self._src_size) * 100
				))
				"""
				self.log("%1d/%1d: %s -> %s (%1.2f%%)" % (
					cp,len(pages),size_str(ofs),size_str(ds),
					ds / float(ofs) * 100
				))

			#cleanup:
			self.statustext("Cleanup...")
			pb = None
			if os.path.isfile(tmp):
				os.remove(tmp)
			else:
				print "failed to cleanup temp file: '%s'" % tmp
			gc.collect()

			#stats:
			with self.lock:
				self._pages_processed = self._pages_processed + 1
				self._total_pages_processed = self._total_pages_processed + 1

			#if cp>10: break

		#close files:
		self.extractor.close()
		zfile.close()
		gc.collect()

		if set_ok and ok:
			self.set_ok()

	def set_ok(self):
		self.statustext("All Done! If you survive, please come again!")
		self.status(Crusher.OK)



def is_image_file(path):
	"""Return True if the file at <path> is an image file recognized by PyGTK.
	"""
	if os.path.isfile(path):
		info = gtk.gdk.pixbuf_get_file_info(path)
		return info is not None
	return False

def alphanumeric_sort(filenames):
	"""Do an in-place alphanumeric sort of the strings in <filenames>,
	such that for an example "1.jpg", "2.jpg", "10.jpg" is a sorted
	ordering.
	"""
	def _format_substring(s):
		if s.isdigit():
			return int(s)
		return s.lower()

	rec = re.compile("\d+|\D+")
	filenames.sort(key=lambda s: map(_format_substring, rec.findall(s)))

def size_str(size = 0,places=2,base=1024):
	size = float(size)
	unit = "bytes"
	units = ["KB","MB","GB","TB","PB"]
	while size > base:
		size = size / base
		unit = units.pop(0)
		if len(units) == 0: break;

	formatstr = "%%1.%1df" % places
	ret = formatstr % size
	return "%s %s" % (ret,unit)
