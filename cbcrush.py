#!/usr/bin/env python
"""
ComixCrusher

An application to optimize comic book files for disk space/screen size,
	resizing images and re-saving the archive (to a new destination) with
	high compression

NOTE: THIS IS A LOSSY PROCESS! SOME TEXT MAY BECOME UNREADABLE!

"""


import gobject
import gtk
import pango

import time

#import encoding
#import image
#import Image

from const import *

from crusherclass import *


class CrusherWindow(gui.GtkBetterDialog):
	def __init__(self):
		self._width=800
		self._widewidth=1600
		
		gui.GtkBetterDialog.__init__(self,"%s v%1.2f" % (APPNAME,VERSION))
		#needed so that threads can run at same time as gtk
		gobject.threads_init()

		self.set_size_request(800,480)

		#our crusher object which does the hard work:
		self.crusher = Crusher()
		
		#set defaults based on screen size:
		self.widewidth(self.width(gtk.gdk.screen_width())*2)

		self.notebook = gtk.Notebook()
		self.add_row(self.notebook)

		self.main_page = page = gtk.VBox()
		self.notebook.append_page(page,gtk.Label("1. Config"))
		self.add_label("<b>%s</b>" % APPNAME,0,5,page)
		self.add_label("""<i>This tool shrinks your comic books by resizing the images to a size more appropriate to portable devices.

<b>Note that this is a lossy process!</b> Some small text may become unreadable if the images are
scaled down too much! You should always keep a backup of the original files.
You are encouraged to experiment with the settings to get an optimal balance between file size and
image quality.
</i>
""",0,5,page)

		hbox = gtk.HBox()

		self.widthbox = gui.GtkPropertyEditor(self.width,int,
			"Shrink normal pages to"
		)
		self.widthbox.control.set_range(640,1920)
		self.widthbox.control.set_increments(20,100)
		hbox.pack_start(self.widthbox)
		hbox.pack_start(gtk.Label("pixels wide."),expand=False)
		self.add_row(hbox,page)

		hbox=gtk.HBox()

		self.wide_control=gui.GtkPropertyEditor(self.widewidth,int,
			"Scale wide (ie double) pages to"
		)
		hbox.pack_start(self.wide_control)
		hbox.pack_start(gtk.Label("pixels wide."),expand=False)

		self.add_row(hbox,page)

		self.width_label = self.add_label("<i>Max image width will be 1000px</i>",1,5,page)

		self.jpg_quality_slider = gui.GtkPropertyEditor(
			self.crusher.jpg_quality, "scale:25:100:0", "JPEG Quality:"
		)
		#hbox.pack_start(self.jpg_quality_slider)

		self.add_row(self.jpg_quality_slider,page)

		#options to discard or keep non-image files:
		hbox=gtk.HBox()
		self.discard_non_image_files=gtk.RadioButton()
		self.discard_non_image_files.set_label("Discard non-image files")
		self.discard_non_image_files.set_active(True)
		hbox.pack_start(self.discard_non_image_files)
		self.keep_non_image_files=gtk.RadioButton(self.discard_non_image_files)
		self.keep_non_image_files.set_label("Keep non-image files")
		hbox.pack_start(self.keep_non_image_files)

		self.add_row(hbox,page)


		#files page
		self.files_page = page = gtk.VBox()
		self.notebook.append_page(page,gtk.Label("2. Choose Files"))

		#columns: selected, source_file, dest_file, filesize,dest_size (with %)
		self.liststore = gtk.ListStore(bool,str,str,str,str)
		self.tree = gtk.TreeView(self.liststore)
		sw = gtk.ScrolledWindow()
		sw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
		sw.add(self.tree)

		self.tree.set_size_request(800,300)
		self.toggler = gtk.CellRendererToggle()
		self.toggler.connect("toggled",self.toggler_cb)
		c = gtk.TreeViewColumn("", self.toggler,active=0)

		self.tree.append_column(c)

		cr = gtk.CellRendererText()
		cr.set_property("editable",True)
		cr.connect('edited',self.filename_edited,1)
		c = gtk.TreeViewColumn("Filename", cr,text=1)
		c.set_sort_column_id(1)
		c.set_min_width(300)
		c.set_max_width(500)
		self.tree.append_column(c)

		cr = gtk.CellRendererText()
		cr.set_property("editable",True)
		cr.connect('edited',self.filename_edited,2)
		c = gtk.TreeViewColumn("Destination", cr,text=2)
		c.set_sort_column_id(2)
		c.set_min_width(300)
		c.set_max_width(500)
		self.tree.append_column(c)
		#read-only cols:
		self.tree.append_column(gtk.TreeViewColumn("Size", gtk.CellRendererText(),text=3))
		self.tree.append_column(gtk.TreeViewColumn("Destination Size", gtk.CellRendererText(),text=4))

		self.add_label("Select one or more files to crush:",0,5,page)
		page.pack_start(sw)

		pane = gtk.HBox()
		#pane.pack_start(gtk.Label("Add/remove etc Buttons Go Here!"))
		self.add_files_button=gtk.Button()
		self.add_files_button.set_label("Add Files...")
		self.add_files_button.connect("clicked",self.add_files)
		pane.pack_start(self.add_files_button)
		self.add_dir_button=gtk.Button()
		self.add_dir_button.set_label("Add Directory...")
		self.add_dir_button.connect("clicked",self.add_dir)
		pane.pack_start(self.add_dir_button)

		self.go_button = gtk.Button()
		self.go_button.set_label("Go!")
		self.go_button.connect("clicked",self.go)
		pane.pack_end(self.go_button)

		self.add_row(pane,page)


		#progress pane with image preview and 2 progress bars
		self.progress_page = page = gtk.VBox()
		self.notebook.append_page(page,gtk.Label("3. Progress"))

		hbox = gtk.HBox()
		self.status_text = self.add_label("Status: Idle.",0,5,hbox)
		self.cancel_button = gtk.Button()
		self.cancel_button.set_label("Stop!")
		self.cancel_button.set_sensitive(False)
		self.cancel_button.connect("clicked",self.cancel_pressed)

		hbox.pack_end(self.cancel_button,False)
		self.add_row(hbox,page)

		self.pixbuf = gtk.Image()

		hbox = gtk.HBox()

		vb = gtk.VBox()
		#vb.pack_start(gtk.Label("Messages:"),expand=False)

		self.logtext = gui.GtkMultiLineTextBox()
		self.logtext.set_size_request(250,100)
		self.logtext.readonly(True)

		vb.pack_start(self.logtext, True, True, 5)
		hbox.pack_start(vb, True, True, 5)

		hbox.pack_start(self.pixbuf, True, True, 5)

		page.pack_start(hbox, True, True, 5)

		self.thumb_size=250

		self.pixbuf.set_from_stock(gtk.STOCK_DIALOG_INFO,gtk.ICON_SIZE_DIALOG)
		self.pixbuf.set_size_request(self.thumb_size,self.thumb_size)

		self.itm_progress = gtk.ProgressBar()
		self.total_progress = gtk.ProgressBar()
		self.itm_label = self.add_label("Item Progress:",0,5,page)
		self.add_row(self.itm_progress,page)
		self.total_label = self.add_label("Overall Progress:",0,5,page)
		self.add_row(self.total_progress,page)
		self.last_update = time.time()


		self.show_all()

	def cancel_pressed(self,*args):
		self.crusher.cancelled(True)
		#self.cancel_button.set_sensitive(False)

	def add_files(self,*args):
		src = gui.GtkFileChooserWidget.run_dialog(["*.cbz","*.cbr"],None,"Choose a File to Crush","Comic Book Files")
		if os.path.exists(src):
			root,ext = os.path.splitext(src)
			new_filename = "%s-crushed.cbz" % root
			#print "default: %s" % new_filename
			dest = gui.GtkFileChooserWidget.run_dialog(["*.cbz","*.cbr"],new_filename,"Choose a destination file","Comic Book Files",True)
			if dest != "":
				root,ext = os.path.splitext(dest)
				dest = "%s.cbz" % root
				self.add_file(src,dest)

	def add_file(self,src,dest,selected=True):
		"""
		Adds an file / destination combo to the list store
		"""
		filesize = os.path.getsize(src)
		filesize = "%1.2fMB" % (float(filesize) / 1024 / 1024)
		destfs=""
		if os.path.exists(dest):
			destfs = os.path.getsize(dest)
			destfs = "%1.2fMB" % (float(destfs) / 1024 / 1024)
		self.liststore.append([selected,src,dest,filesize,destfs])


	def add_dir(self,*args):
		scanner = DirScanner()
		matches = scanner.matches

		#for f in scanner.matches:
		#	print f
		print "%1d files found." % len(matches)
		alphanumeric_sort(matches)

		files = ()
		for f in matches:
			bn = os.path.basename(scanner.scanpath)
			dn = os.path.dirname(scanner.scanpath)
			destdir = os.path.join(dn,"%s-crushed" % bn)
			destfile = f.replace(scanner.scanpath,destdir)
			#replace extension with .cbz:
			fn,ext = os.path.splitext(destfile)
			destfile="%s.cbz" % fn
			self.add_file(f,destfile)

		#TODO: prompt for destination dir and build dest filenames


	def progress_report(self,*args):
		#ensure that we're looking at the progress pane, and
		# update its controls with current crusher state
		# this is called regularly when the crusher is busy
		"""
		state struct:
			"status": self._status,
			"statustext": self._statustext,
			"pages_processed": self._pages_processed,
			"crushed": self._crushed,
			"skipped": self._skipped,
			"discarded": self._discarded,
			"src_size": self._src_size,
			"dest_size": self._dest_size
		"""

		self.notebook.set_page(2) #progress
		state = self.crusher.state()
		statustext = state['statustext']
		status = Crusher.state_str(state['status'])
		self.status_text.set_markup("Status: <b>%s</b> <i>(%s)</i>" % (status,statustext))

		msg = "Processing %s" % (
			state['current_file']
		)
		self.itm_label.set_text(msg)

		pct=0
		if int(state['src_size']) > 0 and int(state['pages_in_file']) > 0:
			pct = float(state['pages_processed']) / float(state['pages_in_file'])
			self.itm_progress.set_fraction(pct)
			msg = "%s of %s pages processed, %s -> %s (%1.2f%%)" % (
				state['pages_processed'], state['pages_in_file'],
				size_str(state['src_size']),
				size_str(state['dest_size']),
				(float(state['dest_size']) / float(state['src_size'])) * 100
			)
			self.itm_progress.set_text(msg)

		if int(state['files_to_process']) > 0:
			pct = (float(state['files_processed']) + pct) / float(state['files_to_process'])
			if pct > 1: pct = 1
			self.total_progress.set_fraction(pct)
			msg = "Crushing file '%s' (%1d of %1d)" % (
				os.path.basename(state['current_filename']),
				int(state['files_processed'])+1,
				state['files_to_process']
			)
			if int(state['total_in']) > 0:
				inbytes = size_str(state['total_in'])
				outbytes = size_str(state['total_out'])
				pcnt = float(float(state['total_out']) / float(state['total_in']) * 100)
				lbl = "Overall Progress: <b>%s</b> in, <b>%s </b>out. Compression Ratio: <b>%1.2f%%</b>" % (
					inbytes,outbytes,pcnt
				)
				self.total_label.set_markup(lbl)
			self.total_progress.set_text(msg)


		if state['pixbuf'] is not None:
			pb = state['pixbuf']
			#remove the pixbuf to save repeated renders of the same image
			self.crusher.clear_pixbuf()
			h = pb.get_height()
			w = pb.get_width()
			nh=self.thumb_size
			nw = (w/float(h)) * nh
			pb = pb.scale_simple(int(nw),int(nh),gtk.gdk.INTERP_NEAREST)
			self.pixbuf.set_from_pixbuf(pb)

		#logtext = self.logtext.text()
		for itm in state["logs"]:
			#info messages do not get 'info:' prefix
			if itm["level"] == Crusher.INFO:
				line = "%s\n" % itm["message"]
			else:
				line = "%s: %s\n" % (
					Crusher.level_str(itm["level"]), itm["message"]
			)
			self.logtext.append(line)

		#scroll messages to bottom:
		self.logtext.scroll_to_end()
		#

		busy = state['status'] == Crusher.BUSY

		self.cancel_button.set_sensitive(busy)

		if not busy:
			if state['status'] == Crusher.OK:
				msg = "Done!"
			else:
				msg = "Crushing incomplete!"

			gui.GtkOKDialog.show(self,msg)
			#process complete - return False to stop the timer
			return False

		#run the timer again:
		return True


	def go(self,*args):
		files = []
		i = self.liststore.get_iter_first()
		while i != None:
			if self.liststore.get_value(i,0): #selected
				src = self.liststore.get_value(i,1)
				if os.path.exists(src):
					files.append({
						'src': src,
						'dst': self.liststore.get_value(i,2)
					})
				else:
					gui.GtkOKDialog.show(self,"Error! '%s' does not exist, skipping" % src)

			i = self.liststore.iter_next(i)

		self.notebook.set_page(2) #progress
		self.gtk_update()
		#set up a timer to update regularly
		gobject.timeout_add(250,self.progress_report)

		self.crusher.cancelled(False)
		self.crusher.crush_in_thread(files)

		#[
		#	{ "src": "/media/2tb_external/data//Tales From The Clerks - The Omnibus Collection (2006) (Digital) (Minutemen-Syl3ntBob).cbz",
		#		"dst": "/home/antisol/clerks.cbz"},
		#
		#])

		#self.crusher.crush_file(
		#	"/media/SD/comics/Tales From The Clerks - The Omnibus Collection (2006) (Digital) (Minutemen-Syl3ntBob).cbz") #,
		#	"/apps/test.cbz")
		#self.crusher.crush_file("/media/SD/comics/X-Men The Dark Phoenix Saga TPB (Bchry-DCP).cbr",
		#	"/apps/Phoenix.cbz")
		#Anne Rice's The Vampire Lestat Vol.1991 #01 (1991).cbz

		#gui.GtkOKDialog.show(self,"Done","Crushing complete!")


	def filename_edited(self,cellrenderertext, path, new_text, colnum):
		iter = self.liststore.get_iter_from_string(path)
		self.liststore.set(iter,colnum,new_text)



	def toggler_cb(self,toggler,path):
		val = not toggler.get_active()
		iter = self.liststore.get_iter_from_string(path)
		self.liststore.set(iter,0,val)

	def widewidth(self,val = None):
		if val != None:
			self._widewidth = int(val)
			self.size_update()
		return self._widewidth

	def width(self,val = None):
		if val != None:
			self._width = int(val)
			self._widewidth = int(val)*2
			self.size_update()
		return self._width

	def size_update(self):
		self.crusher.width(self.width())
		self.crusher.widewidth(self.widewidth())
		
		#gui items may not be created yet
		if hasattr(self,"widthbox"): 
			self.widthbox.refresh()
			
		if hasattr(self,"wide_control"): 
			self.wide_control.refresh()
		
		if hasattr(self,"width_label"): 
			self.width_label.set_markup("Pages will be no more than <b>%1dpx</b> wide.\nWide pages will be no more than <b>%1dpx</b> wide." % (self.crusher.width(),self.widewidth()))

	def gtk_update(self, force=True):
		"""
		This is how we get around calling gtk_main or running in another
			thread. This needs to happen regularly
		"""

		# limit frame rate
		if not force and (time.time() - self.last_update < 0.2):
			return False

		gtk.main_iteration(False)
		while gtk.events_pending():
				gtk.main_iteration(False)

		self.last_update = time.time()


# scans a directory recursively for supported files
class DirScanner:
	def find_instances(self, widgets,classname):
		"""
		given a bunch of widgets (return from gtk.container.get_children())
			recursively walks through hierarchy
			and returns all GtkTypeSelectors as an array
		"""
		ret = []
		for widget in widgets:
			if isinstance(widget, classname):
				ret.append(widget)
			elif isinstance(widget, gtk.Container):
				ret = ret + self.find_instances(widget.get_children(),classname)

		return ret

	def __init__(self):
		"""
		Pops up a customized GTK directory chooser
			with widgets for selecting supported filetypes
		Then scans the chosen directory and returns a bunch of
			Application objects.
		"""
		self.window = False
		path = ""

		# directory chooser dialog
		dialog = gtk.FileChooserDialog(
			"Choose a directory to scan:",
			action=gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,
			buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_OPEN, gtk.RESPONSE_OK))

		response = dialog.run()
		self.response = response
		if response == gtk.RESPONSE_OK:
			path = dialog.get_filename()
			dialog.destroy()

			self.matches = []
			if path:
				self.scanpath = path
				self.matches = self.scan(path)
		else:
			dialog.destroy()

	def scan(self, path):

		self.progress(0, path)

		matches = self.scan_r(path)

		self.window.destroy()
		self.window.gtk_update()
		self.window = False

		print  "Scan complete, Found %1d Files." % len(matches)

		return matches

	def scan_r(self, scanpath, baseval=0.0, maxval=100.0):
		"""
		Recursive directory scan with clever math for updating a progressbar
			scanpath	 directory to scan
			filters		array of FileFilters to run files through
			baseval		progress percentage at start of scan
			maxval		progress percentage at end of scan
		"""
		items = os.listdir(u"%s" % scanpath)
		matches = []

		if len(items) == 0: return matches

		position = float(baseval)
		each_item = (maxval - baseval) / float(len(items))

		# if max <= 0:
		# 	max = 1
		file_re = re.compile(r'\.(cbz|cbr)\s*$', re.I)
		for item in items:
			path = os.path.join(scanpath, item)
			if os.path.isdir(path):
				self.progress(position / 100.0, path)
				#recurse:
				matches = matches + self.scan_r(
					path, position, position + each_item
				)
			else:
				if re.search(file_re,path):
					# too slow:
					#if archive.get_archive_info(path) != None:
					self.progress(position / 100.0, path)
					matches.append(path)

			# we don't call progress() for each item because it's slow
			position += each_item

		return matches


	def create_window(self):
		self.window = gui.GtkProgressDialog("Scanning...")

	def progress(self, fraction=0, text=None):

		if not self.window:
			self.create_window()

		self.window.update(float(fraction), text)
		#self.window.gtk_update()


if __name__ == '__main__':
	dialog = CrusherWindow()
	dialog.run()
	#app exited, kill running process:
	dialog.crusher.cancelled(True)
	dialog.destroy()

